const fs  = require("fs");
const sim = require("string-similarity");
const yml = require("yaml");
const csv = require('csv-writer');

const getDirCodes = dir => {
    const dirCodes = {};

    fs.readdirSync(dir).forEach(filename => {
        const path = `${dir}/${filename}`;

        if (fs.lstatSync(path).isFile()) {
            dirCodes[filename] = fs.readFileSync(path).toString();
        }
    });

    return dirCodes;
};

const [input, output] = [yml.parse(fs.readFileSync('data/input.yml', 'utf8')), []];

input.dirsToCompare.forEach(volume => {
    const source = volume.split(":")[0];
    const target = volume.split(":")[1];
    const sourcePath = `${input.sourceBaseDir}/${source}`;
    const targetPath = `${input.targetBaseDir}/${target}`;

    const [raw, final] = [getDirCodes(sourcePath), getDirCodes(targetPath)];
    
    const temp = Object.keys(raw)
        .filter(k => k in final)
        .map(k => ({
            file: `${target}/${k}`,
            lines1: raw[k].split(/\r\n|\r|\n/).length,
            lines2: final[k].split(/\r\n|\r|\n/).length,
            sim: (sim.compareTwoStrings(raw[k], final[k]) * 100).toFixed(2)
        }));

    output.push(...temp);
});

["Models", "Controllers", "Services", "Data", "Views"].forEach(e => {
    const groupOutput = output.filter(o => o.file.includes(e));
    const outputFileName = `data/output-${e.toLowerCase()}.csv`;
    const writer = csv.createObjectCsvWriter({
        path: outputFileName,
        header: [
            { id: 'file', title: 'Arquivo' },
            { id: 'lines1', title: 'Linhas geradas' },
            { id: 'lines2', title: 'Linhas cód. final'},
            { id: 'sim', title: 'Aproveitamento (%)' }
        ]
    });
    
    writer.writeRecords(groupOutput)
        .then(() => console.log(`✅ ${outputFileName}`));
    
    const groupAverage = groupOutput
        .map(e => parseInt(e.sim))
        .reduce((a, b) => a + b) / groupOutput.length;

    console.log(`Média de aproveitamento [${e}]: ${groupAverage.toFixed(2)}%`);
});

const totalAverage = output
    .map(e => parseInt(e.sim))
    .reduce((a, b) => a + b) / output.length;

console.log(`Média de aproveitamento [Total]: ${totalAverage.toFixed(2)}%`);