﻿As variáveis de ambiente abaixo precisam ser configuradas no `appSettings.json` ou `docker-compose.yml` para que os serviços de **E-mails** e **Logging** funcionem adequadamente:

```json
"EmailSender": {
	"Host": "smtp.mailtrap.io",
	"Port": "2525",
	"EnableSSL": "True",
	"FromEmail": "no-reply@scap.dev",
	"UserName": "USER_NAME",
	"Password": "PASSWORD"
},
"Kisslog": {
	"OrganizationId": "ORGANIZATION_ID",
	"ApplicationId": "APPLICATION_ID",
	"ApiUrl": "https://api.kisslog.net"
}
```

As configurações já definidas no `docker-compose.yml` servem exclusivamente para fins de testes e apresentação do PG. Após o dia 13/05/2021, não há garantia de que as chaves continuem funcionando, visto que estas são de cunho pessoal do autor do trabalho e tendem a serem revogadas.