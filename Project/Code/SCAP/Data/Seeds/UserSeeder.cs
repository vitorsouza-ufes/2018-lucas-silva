﻿using Microsoft.AspNetCore.Identity;
using SCAP.Models;

namespace SCAP.Data.Seeds
{
    public static class UserSeeder
    {
        public static void SeedDev(UserManager<Pessoa> userManager)
        {
            var users = new Pessoa[]
            {
                new Secretario
                {
                    UserName = "2016101236",
                    Email = "lucasribeiromsr@gmail.com",
                    EmailConfirmed = true,
                    PhoneNumber = "27997999014",
                    PhoneNumberConfirmed = true,
                    Nome = "Lucas",
                    Sobrenome = "Silva",
                    Ativo = true
                },
                new Secretario
                {
                    UserName = "2014100465",
                    Email = "eric.marchetti@fake.com",
                    EmailConfirmed = true,
                    PhoneNumber = "27999229999",
                    PhoneNumberConfirmed = true,
                    Nome = "Eric",
                    Sobrenome = "Marchetti",
                    Ativo = true
                },
                new Professor
                {
                    UserName = "2016101214",
                    Email = "fernando.amaral@fake.com",
                    EmailConfirmed = true,
                    PhoneNumber = "27998024999",
                    PhoneNumberConfirmed = true,
                    Nome = "Fernando",
                    Sobrenome = "Amaral",
                    Ativo = true
                },
                new Professor
                {
                    UserName = "2016101224",
                    Email = "lucas.tassis@fake.com",
                    EmailConfirmed = true,
                    PhoneNumber = "27997873999",
                    PhoneNumberConfirmed = true,
                    Nome = "Lucas",
                    Sobrenome = "Tassis",
                    Ativo = true
                },
                new Professor
                {
                    UserName = "2016101246",
                    Email = "breno.aguiar@fake.com",
                    EmailConfirmed = true,
                    PhoneNumber = "27999001999",
                    PhoneNumberConfirmed = true,
                    Nome = "Breno",
                    Sobrenome = "Aguiar",
                    Ativo = true
                },
                new Professor
                {
                    UserName = "2016204404",
                    Email = "dhiego.broetto@fake.com",
                    EmailConfirmed = true,
                    PhoneNumber = "27995003999",
                    PhoneNumberConfirmed = true,
                    Nome = "Dhiego",
                    Sobrenome = "Broetto",
                    Ativo = true
                },
                new Professor
                {
                    UserName = "2016101235",
                    Email = "eduardo.montagner@fake.com",
                    EmailConfirmed = true,
                    PhoneNumber = "27992585999",
                    PhoneNumberConfirmed = true,
                    Nome = "Eduardo",
                    Sobrenome = "Montagner",
                    Ativo = true
                },
                new Professor
                {
                    UserName = "2016101217",
                    Email = "pedro.flores@fake.com",
                    EmailConfirmed = true,
                    PhoneNumber = "27996360999",
                    PhoneNumberConfirmed = true,
                    Nome = "Pedro",
                    Sobrenome = "Flores",
                    Ativo = true
                }
            };

            AddUsers(userManager, users);
        }
        
        public static void SeedStagingOrProduction(UserManager<Pessoa> userManager)
        {
            var users = new Pessoa[]
            {
                new Secretario
                {
                    UserName = "2016101236",
                    Email = "lucasribeiromsr@gmail.com",
                    EmailConfirmed = true,
                    PhoneNumber = "27997999014",
                    PhoneNumberConfirmed = true,
                    Nome = "Lucas",
                    Sobrenome = "Silva",
                    Ativo = true
                }
            };

            AddUsers(userManager, users);
        }

        private static void AddUsers(UserManager<Pessoa> userManager, Pessoa[] users)
        {
            foreach (var user in users)
            {
                if (userManager.FindByNameAsync(user.UserName).Result != null)
                {
                    continue;
                }

                var result = userManager.CreateAsync(user, "senha@123").Result;

                if (result.Succeeded)
                {
                    if (user is Secretario)
                    {
                        userManager.AddToRoleAsync(user, "Secretario").Wait();
                    }

                    if (user is Professor)
                    {
                        userManager.AddToRoleAsync(user, "Professor").Wait();
                    }
                }
            }
        }
    }
}
