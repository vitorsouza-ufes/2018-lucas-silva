% ==============================================================================
% TCC - Nome do Aluno
% Capítulo 1 - Introdução
% ==============================================================================
\chapter{Introdução}
\label{sec-intro}

Com a ascenção da Internet como um dos principais meios de comunicação existentes no mundo, em especial a \textit{World Wide Web} como plataforma para implantação de Sistemas de Informação Web (\textit{Web Information Systems} ou WISs), surgiu a demanda por \textit{softwares} de qualidade que atendam requisitos como segurança, usabilidade, portabilidade, desempenho e outros. Consequentemente, através de conceitos de Engenharia de Software, foram criados métodos e ferramentas para auxiliarem no processo de desenvolvimento, implantação e manutenção desses \textit{softwares}, inaugurando, então, a Engenharia Web~\cite{souza-falbo:webmedia05}.

Durante o processo de desenvolvimento de WISs, é comum a utilização de \textit{frameworks}, artefatos de código que proveem componentes prontos para reuso. Porém, apesar de terem influência direta na arquitetura dos sistemas, é comum que não sejam considerados até a fase de implementação. Foi proposto, então, o FrameWeb~\cite{souza-et-al:iism09}, um método baseado em \textit{frameworks} para desenvolvimento de WISs. FrameWeb define uma arquitetura padrão para facilitar a integração com \textit{frameworks} de desenvolvimento Web, além de propor uma linguagem~\cite{martins-souza:webmedia15} para construção de um conjunto de modelos, que trazem para o projeto arquitetural do sistema conceitos inerentes a estes \textit{frameworks}.

Foram desenvolvidas duas ferramentas de apoio ao método, sendo elas um editor de diagramas e um gerador de código. O \textit{FrameWeb Editor}~\cite{campos-souza:webmedia17} permite criar diagramas que atendam aos modelos propostos pelo método, de forma gráfica. Já o gerador de código~\cite{almeida-et-al:webmedia17,zupeli-souza:webmedia18} é capaz realizar transformações dos modelos criados no \textit{FrameWeb Editor} em código fonte.

O uso de técnicas MDD (\textit{Model-Driven Development}) adotadas pelas ferramentas associadas ao FrameWeb permitem que seus modelos utilizem conceitos de categorias de \textit{frameworks} (ex.: mapeamento objeto/relacional) sem que se comprometam com \textit{frameworks} específicos (ex.: Hibernate, Eloquent, Sequelize, etc.) até a fase de implementação. O gerador de código, por sua vez, pode ser estendido através de \textit{templates}. Estes servem como base para a geração de código em diferentes linguagens e \textit{frameworks}, que refletem os modelos FrameWeb construídos no editor.

Neste trabalho será desenvolvida uma nova revisão e implementação do Sistema de Controle de Afastamentos de Professores (SCAP), assim como foi feito em~\cite{bduarte:jee7} e~\cite{rprado:vraptor}, utilizando o método FrameWeb, apoiado por seu conjunto de ferramentas, mas atendendo à plataforma ASP.NET Core~\cite{aspnet:core}, de modo a avaliar a viabilidade e aplicabilidade do método e seu ferramental a esta plataforma.

%%% Início de seção. %%%
\section{Motivação e Justificativa}
\label{sec-intro-motivacao-justificativa}

Para avaliar a aplicabilidade do FrameWeb em diversas plataformas e conjuntos de \textit{frameworks}, foram modelados e desenvolvidos WISs para estas plataformas. Em particular, \citeonline{bduarte:jee7} e \citeonline{rprado:vraptor} realizaram o levantamento e análise de requisitos do SCAP, desenvolvendo-o através do método FrameWeb, utilizando a plataforma Java EE 7 e o \textit{framework} VRaptor 4, respectivamente. 

Sucessivamente, outros trabalhos reimplementaram o SCAP utilizando os \textit{frameworks} Java Spring MVC e Vaadin~\cite{rmatos:spring-vaadin}, Ninja~\cite{ravelar:ninja}, Wicket e Tapestry~\cite{mferreira:wicket-tapestry} e Play~\cite{cguterres:play} na plataforma Java, bem como os \textit{frameworks} Laravel~\cite{fpinheiro:laravel}, Code Igniter e Node.JS~\cite{lmeirelles:igniter-node} em outras plataformas. Estes trabalhos trazem análises do método FrameWeb através do desenvolvimento do SCAP utilizando os \textit{frameworks} escolhidos.

ASP.NET Core~\cite{aspnet:core} vem ganhando destaque e crescendo cada vez mais no mercado de WISs.\footnote{\url{https://insights.stackoverflow.com/survey/2020/#most-popular-technologies}} Porém, até a data corrente deste trabalho, não foram feitos experimentos do método FrameWeb nesta plataforma, de modo a avaliar o suporte por seu ferramental. Sendo assim, este trabalho tem como motivação incluir o \textit{framework} da Microsoft na lista de compatibilidade do editor e do gerador de código FrameWeb, por meio da criação de \textit{templates} apropriados para a plataforma e os testando a partir do desenvolvimento do SCAP. Todas as linguagens de programação e \textit{frameworks} suportados pelo FrameWeb Editor estão disponíveis no repositório oficial do projeto.\footnote{\url{https://github.com/nemo-ufes/FrameWeb}}

\section{Objetivos}
\label{sec-intro-obj}

Diferente de trabalhos passados, feitos majoritariamente em Java e sem o apoio das ferramentas FrameWeb, este trabalho tem como objetivo geral aplicar o método FrameWeb, com apoio de seu conjunto de ferramentas, em uma nova implementação do SCAP, mas desta vez feita em C\#, utilizando a plataforma ASP.NET Core. Baseando-se nos requisitos levantados por \citeonline{bduarte:jee7} e \citeonline{rprado:vraptor}, a documentação do SCAP será revisada, quando necessário, de modo a torná-la mais consistente e detalhada.

Os objetivos principais deste trabalhos são: prover análises dos resultados alcançados nas etapas de modelagem e geração de código até a total implementação do SCAP, utilizando ASP.NET Core, indicando as vantagens e desvantagens do uso do método FrameWeb e suas ferramentas no desenvolvimento de um WIS, propondo melhorias e novas funcionalidades para trabalhos futuros.

%%% Início de seção. %%%
\section{Método de Desenvolvimento do Trabalho}
\label{sec-intro-met}

Para alcançar os objetivos propostos, foram desenvolvidas as seguintes atividades:

\begin{itemize}
	\item \textbf{Atividade 1}
	
	Estudo do método FrameWeb~\cite{souza-et-al:iism09} e de seu ferramental~\cite{martins-souza:webmedia15, campos-souza:webmedia17, zupeli-souza:webmedia18}, revisão dos padrões e técnicas de Engenharia de Software presentes nas notas de aula do professor doutor Ricardo de A. Falbo (\citeyear{rfalbo:naes},~\citeyear{rfalbo:naer},~\citeyear{rfalbo:naps}), estudo dos trabalhos de experimentação do método FrameWeb, em especial os resultados e documentações levantadas por~\citeonline{bduarte:jee7} e~\citeonline{rprado:vraptor}.
	
	\item \textbf{Atividade 2}
	
	Elaboração dos documentos de Definição e Especificação de Requisitos do SCAP. Ambos documentos foram baseados nos trabalhos de~\citeonline{bduarte:jee7} e~\citeonline{rprado:vraptor}, sendo revisadas as partes que apresentavam incompletudes ou inconsistências.
	
	\item \textbf{Atividade 3}
	
	Elaboração dos \textit{templates} FrameWeb responsáveis pela transformação M2T (\textit{Model to Text}) dos diagramas feitos no \textit{FrameWeb Editor} para código fonte em C\#. Os códigos gerados foram analisados e executados na plataforma ASP.NET Core para fins de comprovação de funcionamento e equivalência com os modelos de teste.
	
	\item \textbf{Atividade 4}
	
	Através do FrameWeb Editor, foram feitos os modelos FrameWeb	 referentes ao SCAP, obedecendo ao máximo a documentação da aplicação.
	
	\item \textbf{Atividade 5}
	
	Com o código gerado, o ambiente de desenvolvimento foi preparado para que o sistema pudesse ser implementado. Enquanto a aplicação estava em processo de desenvolvimento, foram feitas análises de eficiência da utilização do método FrameWeb e de seu ferramental.
	
	\item \textbf{Atividade 6}
	
	Elaboração da Monografia e da apresentação final, baseadas nos conhecimentos adquiridos durante o curso de Ciência da Computação e do Projeto de Graduação.
	
	\item \textbf{Atividade 7}
	
	Apresentação do trabalho para a banca.
	
	\item \textbf{Observações}
	
	Para o desenvolvimento e organização do trabalho, foi empregado o uso do Kanban, técnica que consiste num sistema de controle e gestão do fluxo de trabalho por meio do uso de cartões. Subdividindo as tarefas descritas anteriormente em tarefas menores, estas foram alocadas num quadro \emph{To-do}, indicando que deveriam ser feitas. Quando iniciada uma tarefa, esta era realocada para o quadro \emph{In progress}, indicando que estava sendo desenvolvida. Uma vez finalizada, a tarefa tinha como destino o quadro \emph{Done}, assinalando seu término. O Trello~\cite{trello:kanban} foi escolhido como ferramenta de auxílio na manutenção do quadro Kanban.
\end{itemize}


\section{Organização da Monografia}
\label{sec-organizacao-monografia}

Esta monografia foi particionada em 5 diferentes partes, incluindo este capítulo de Introdução. O Capítulo~\ref{sec-referencial} traz informações que dizem respeito ao referencial teórico utilizado como base para a elaboração deste trabalho.

O Capítulo~\ref{sec-requisitos} detém informações sucintas sobre a documentação de requisitos do WIS desenvolvido neste trabalho, documentação esta que passou por revisões e aprimoramentos para melhor atender os objetivos do trabalho.

O Capítulo~\ref{sec-projeto} aborda o projeto arquitetural e as etapas de implementação do SCAP, contendo também informações que dizem respeito às diferentes tecnologias utilizadas para melhor alcançar os resultados esperados, assim como o processo de modelagem e geração de código utilizando o FrameWeb e suas ferramentas.

O Capítulo~\ref{sec-conclusoes} conclui este trabalho, trazendo as impressões sobre a experiência de desenvolvimento de um WIS, intermediada pelo método FrameWeb, falando sobre os resultados alcançados e problemas encontrados, assim como propostas de melhorias para o aprimoramento do método e seu ferramental.

% Comment
\iffalse

%%% Início de seção. %%%
\section{Listagens de código}
\label{sec-intro-listagens}

O pacote \texttt{listings}, incluído neste template, permite a inclusão de listagens de código. Análogo ao já feito anteriormente, listagens possuem rótulos para que possam ser referenciadas e sugerimos uma regra de nomenclatura para tais rótulos: usar como prefixo o rótulo do capítulo, substituindo \texttt{sec-} por \texttt{lst-}.

A Listagem~\ref{lst-intro-exemplo}, por exemplo, possui o rótulo \texttt{lst-intro-exemplo} e representa o código que foi usado no próprio documento para exibir as listagens desta seção. Como podemos ver, a sugestão é que os arquivos de código sejam colocados dentro da pasta \texttt{codigos/} e tenham nome idêntico ao rótulo, colocando a extensão adequada ao tipo de código.

\lstinputlisting[label=lst-intro-exemplo, caption=Exemplo de código \latex para inclusão de listagens de código., float=htpb]{codigos/lst-intro-exemplo.tex}

A Listagem~\ref{lst-intro-outroexemplo} mostra um exemplo de listagem com especificação da linguagem utilizada no código. O pacote \texttt{listings} reconhece algumas linguagens\footnote{Veja a lista de linguagens suportadas em \url{http://en.wikibooks.org/wiki/LaTeX/Source\_Code\_Listings\#Supported_languages}.} e faz ``coloração'' de código (na verdade, usa \textbf{negrito} e não cores) de acordo com a linguagem. O parâmetro \texttt{float=htpb} incluído em ambos os exemplos impede que a listagem seja quebrada em diferentes páginas.

\lstinputlisting[label=lst-intro-outroexemplo, caption=Exemplo de código \java especificando linguagem utilizada., language=Java, float=htpb]{codigos/lst-intro-outroexemplo.java}



%%% Início de seção. %%%
\section{Figuras}
\label{sec-intro-figuras}

Figuras podem ser inseridas no documento usando o \emph{ambiente} \texttt{figure} (ou seja, \texttt{\textbackslash begin\{figure\}} e \texttt{\textbackslash end\{figure\}}) e o comando \texttt{\textbackslash includegraphics\{\}}. Existem alguns outros elementos e propriedades úteis de serem configuradas, resultando no código exibido na Listagem~\ref{lst-intro-figuras}.

\lstinputlisting[label=lst-intro-figuras, caption=Código \latex utilizado para inclusão das figuras na Seção~\ref{sec-intro-figuras}., float=htpb]{codigos/lst-intro-figuras.tex}

O comando \texttt{\textbackslash centering} centraliza a figura na página. A opção \texttt{width} do comando \texttt{\textbackslash includegraphics\{\}} determina o tamanho da figura e usa-se \texttt{\textbackslash textwidth} (opcionalmente multiplicado por um número) para se referir à largura da página.

O parâmetro do comando \texttt{\textbackslash includegraphics\{\}} indica onde a imagem pode ser encontrada. Foi criado o diretório \texttt{figuras/} para conter as figuras do documento, dando uma melhor organização aos arquivos. Ao abrir esta pasta, repare que as figuras possuem duas versões---uma em \texttt{.eps} e outra em \texttt{.pdf}---e que o comando \texttt{\textbackslash includegraphics\{\}} não especifica a extensão. Isso se dá porque o \latex possui um compilador para formato PostScript (\texttt{latex}) que espera as imagens em \texttt{.eps} e um compilador para PDF (\texttt{pdflatex}) que espera as imagens em \texttt{.pdf}. Dependendo do seu ambiente \latex, é possível apenas colocar as figuras em formatos mais comuns, como JPG ou PNG e ele incluir no PDF sem problemas. Vale a pena testar.

Por fim, o comando \texttt{\textbackslash caption\{\}} especifica a descrição da figura e \texttt{\textbackslash label\{\}}, como de costume, estabelece um rótulo para permitir referência cruzada de figuras. Note ainda que é utilizada a mesma estratégia de nomenclatura de rótulos usada nas listagens, porém utilizando o prefixo \texttt{fig-}.

As figuras~\ref{fig-intro-nemologo} e~\ref{fig-intro-exemplosideways} mostram o resultado do código da Listagem~\ref{lst-intro-figuras}. A Figura~\ref{fig-intro-exemplosideways}, em particular, utiliza o pacote \texttt{rotating} para mostrar figuras largas em modo paisagem. Basta usar o ambiente \texttt{sidewaysfigure} ao invés de \texttt{figure}.

\begin{figure}
	\centering
	\includegraphics[width=.25\textwidth]{figuras/fig-intro-nemologo} 
	\caption{Exemplo de figura: logo do Nemo.}
	\label{fig-intro-nemologo}
\end{figure}

\begin{sidewaysfigure}
	\centering
	\includegraphics[width=\textwidth]{figuras/fig-intro-exemplosideways} 
	\caption{Exemplo de figura em modo paisagem: um modelo de objetivos~\cite{souza-mylopoulos:spe13}.}
	\label{fig-intro-exemplosideways}
\end{sidewaysfigure}



%%% Início de seção. %%%
\section{Tabelas}
\label{sec-intro-tabelas}

Tabelas são um ponto fraco do \latex. Elas são complicadas de fazer e, dependendo da complexidade da tabela (muitas células mescladas, por exemplo), vale a pena construi-las em outro programa (por exemplo, em seu editor de texto favorito) e inclui-las no documento como figuras. Mostramos, no entanto, alguns exemplos de tabela a seguir. O código utilizado para criar as tabelas encontra-se nas listagens~\ref{lst-intro-tabelas01}, \ref{lst-intro-tabelas02} e~\ref{lst-intro-tabelas03}.

\lstinputlisting[label=lst-intro-tabelas01, caption=Código \latex utilizado para inclusão das tabelas~\ref{tbl-intro-exemplo01} e~\ref{tbl-intro-exemplo02}., float=htpb]{codigos/lst-intro-tabelas01.tex}

\lstinputlisting[label=lst-intro-tabelas02, caption=Código \latex utilizado para inclusão da Tabela~\ref{tbl-intro-exemplo03}., float=htpb]{codigos/lst-intro-tabelas02.tex}

\lstinputlisting[label=lst-intro-tabelas03, caption=Código \latex utilizado para inclusão da Tabela~\ref{tbl-intro-exemplo04}., float=htpb]{codigos/lst-intro-tabelas03.tex}

Em particular, a Tabela~\ref{tbl-intro-exemplo04} utiliza um pacote chamado \texttt{tabularx}, que permite maior controle do layout das tabelas. Ao definir o ambiente \texttt{\textbackslash begin\{tabularx\}}, são definidos os tamanhos de cada coluna proporcional à largura ocupada pela tabela. Veja na Listagem~\ref{lst-intro-tabelas03} que as primeiras duas colunas não definem o atributo \texttt{\textbackslash hsize}, o que faz com que elas fiquem com o tamanho padrão de coluna, que é a largura da tabela dividida pelo número de colunas. Já a terceira coluna define \texttt{\textbackslash hsize=1.2\textbackslash hsize}, ou seja, esta coluna deve ser 20\% maior do que o tamanho padrão. Para isso, é preciso retirar de outras colunas, portanto a quarta e quinta colunas são definidas como 10\% menores (ou seja, \texttt{\textbackslash hsize=0.9\textbackslash hsize}).

% Exemplo de tabela 01:
\begin{table}
	\caption{Exemplo de tabela com diferentes alinhamentos de conteúdo.}
	\label{tbl-intro-exemplo01}
	\centering
	\begin{tabular}{ | c | l | r | p{40mm} |}\hline
		\textbf{Centralizado} & \textbf{Esquerda} & \textbf{Direita} & \textbf{Parágrafo}\\\hline
		C & L & R & Alinhamento de tipo parágrafo especifica largura da coluna e quebra o texto automaticamente.\\
		\hline
		Linha 2 & Linha 2 & Linha 2 & Linha 2\\
		\hline
	\end{tabular}
\end{table}

% Exemplo de tabela 02:
\begin{table}
	\caption{Exemplo que especifica largura de coluna e usa lista enumerada (adaptada de~\cite{souza-mylopoulos:spe13}).}
	\label{tbl-intro-exemplo02}
	\centering
	\renewcommand{\arraystretch}{1.2}
	\begin{small}
		\begin{tabular}{ | p{15mm} | p{77mm} | p{55mm} |}\hline
			\textbf{\textit{AwReq}} & \textbf{Adaptation strategies} & \textbf{Applicability conditions}\\\hline
			
			AR1 &
			\vspace{-2mm}\begin{enumerate}[topsep=0cm, partopsep=0cm, itemsep=0cm, parsep=0cm, leftmargin=0.5cm]
				\item \textit{Warning(``AS Management'')}
				\item \textit{Reconfigure($\varnothing$)}
			\end{enumerate}\vspace{-4mm} &
			\vspace{-2mm}\begin{enumerate}[topsep=0cm, partopsep=0cm, itemsep=0cm, parsep=0cm, leftmargin=0.5cm]
				\item Once per adaptation session;
				\item Always.
			\end{enumerate}\vspace{-4mm}
			\\\hline
			
			AR2 &
			\vspace{-2mm}\begin{enumerate}[topsep=0cm, partopsep=0cm, itemsep=0cm, parsep=0cm, leftmargin=0.5cm]
				\item \textit{Warning(``AS Management'')}
				\item \textit{Reconfigure($\varnothing$)}
			\end{enumerate}\vspace{-4mm} &
			\vspace{-2mm}\begin{enumerate}[topsep=0cm, partopsep=0cm, itemsep=0cm, parsep=0cm, leftmargin=0.5cm]
				\item Once per adaptation session;
				\item Always.
			\end{enumerate}\vspace{-4mm}
			\\\hline
		\end{tabular}
	\end{small}
\end{table}

% Exemplo de tabela 03:
\begin{table}
	\caption{Exemplo que mostra equações em duas colunas (adaptada de~\cite{souza-mylopoulos:spe13}).}
	\label{tbl-intro-exemplo03}
	\centering
	\vspace{1mm}
	\fbox{\begin{minipage}{.98\linewidth}
			\begin{minipage}{0.51\linewidth}
				\vspace{-4mm}
				\begin{eqnarray}
				\Delta \left( I_{AR1} / NoSM \right) \left[ 0, maxSM \right] > 0\\
				\Delta \left( I_{AR2} / NoSM \right) \left[ 0, maxSM \right] > 0\\
				\Delta \left( I_{AR3} / LoA \right) < 0\\
				\end{eqnarray}
				\vspace{-6mm}
			\end{minipage}
			\hspace{2mm}
			\vline 
			\begin{minipage}{0.41\linewidth}
				\vspace{-4mm}
				\begin{eqnarray}
				\Delta \left( I_{AR11} / VP2 \right) < 0\\
				\Delta \left( I_{AR12} / VP2 \right) > 0\\
				\Delta \left( I_{AR6} / VP3 \right) > 0\\
				\end{eqnarray}
				\vspace{-6mm}
			\end{minipage}
	\end{minipage}}
\end{table}

% Exemplo de tabela 04:
\begin{table}[h]
	\caption{Exemplo que utiliza o pacote \texttt{tabularx}, extraído de um artigo ainda não publicado.}
	\label{tbl-intro-exemplo04}
	\centering\tiny\def\tabularxcolumn#1{m{#1}}
	\begin{tabularx}{\columnwidth}{ >{\centering}X | >{\centering}X | >{\hsize=1.2\hsize\centering}X | >{\hsize=0.9\hsize\centering}X | >{\hsize=0.9\hsize\centering\arraybackslash}X }
		\hline
		\textbf{Applied Criteria} & \textbf{Analyzed Content} & \textbf{Initial\\Occurrences} & \textbf{Final Results} & \textbf{Reduction (\%)} \\
		\hline
		Duplicate Removal & Title, authors and year & 903 & 420 & 54,84\% \\ 
		\hline 
		IC and ECs & Title, abstract and keywords & 420 & 130 & 69,05\% \\ 
		\hline 
		IC and ECs & Full text & 130 & 117 & 10\% \\ 
		\hline 
		Final Results & -- & 903 & 117 & 87,04\% \\ 
		\hline 
	\end{tabularx}
\end{table}

\fi