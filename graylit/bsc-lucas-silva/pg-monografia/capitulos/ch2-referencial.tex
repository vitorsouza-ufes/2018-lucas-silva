% ==============================================================================
% TCC - Nome do Aluno
% Capítulo 2 - Referencial Teórico
% ==============================================================================
\chapter{Referencial Teórico}
\label{sec-referencial}

Neste capítulo são apresentados os referenciais teóricos estudados para o desenvolvimento do Projeto de Graduação. A Seção~\ref{sec-engenharia-web} apresenta a Engenharia Web, vertente \textit{Web} da Engenharia de Software. A Seção~\ref{sec-frameworks} aborda os tipos de \textit{frameworks} utilizados para o desenvolvimento de aplicações com o método FrameWeb e que são suportados por suas ferramentas, abordadas na Seção~\ref{sec-frameweb}.

\section{Engenharia Web}
\label{sec-engenharia-web}

Aspectos existentes no ambiente da Web, tais como: concorrência, carga imprevisível, disponibilidade, dinamicidade, segurança, estética e etc.~\cite{pressman2005:eweb} trouxeram novos desafios aos processos já existentes no âmbito de desenvolvimento de \textit{software} para \textit{desktops}, originando uma nova sub-área da Engenharia de Software, chamada de Engenharia Web~\cite{souza:masterthesis07}. A Figura~\ref{pressman-webapp-qualities} apresenta a árvore de critérios de qualidade que um WIS deve possuir, segundo \citeonline{pressman2005:eweb}.

\begin{figure}
	\centering
	\includegraphics[width=.8\textwidth]{figuras/pressman-webapp-qualities.png}
	\caption{Qualidades necessárias para uma aplicação Web~\cite{pressman2005:eweb}.}
	\label{pressman-webapp-qualities}
\end{figure}

A Engenharia Web adota um modelo de projeto divido em diferentes fases, seguindo uma abordagem iterativa. No final de cada etapa, temos sempre artefatos importantes para a construção do WIS e, assim que terminado o projeto, temos o \textit{software} implementado, testado, implantado e atendendo às expectativas do cliente. Um projeto de \textit{software}, por sua vez, pode ainda passar por situações que necessitem de manutenção ou extensão de suas funcionalidades.
	
A primeira etapa para o desenvolvimento de um software é entender quais são as necessidades do cliente, e para alcançar este objetivo é realizada a fase de levantamento de requisitos, junto às partes interessadas, são buscadas todas as informações relevantes possíveis a respeito das funcionalidades que o sistema a ser desenvolvido deve executar (requisitos funcionais), assim como as restrições as quais deve operar (requisitos não funcionais). O produto principal dessa fase é o Documento de Definição de Requisitos~\cite{rfalbo:naer}.

Requisitos Funcionais referem-se às funcionalidades que o sistema deve prover para que atenda às necessidades do cliente. Já os Requisitos Não Funcionais descrevem as exigências técnicas ou restrições as quais o sistema deve operar (Ex: segurança, disponibilidade, portabilidade, etc.). As Regras de Negócio são um conjunto de premissas e restrições encorporadas às funcionalidades do sistema, de forma a satisfazer as políticas do negócio (Ex: leis, regulamentações, padrões de produção, etc.). Há ainda os Requisitos Negativos, responsáveis por registrar particularidades que não fazem parte do escopo do sistema, portanto não devem ser consideradas no desenvolvimento~\cite{rfalbo:naer}.

Identificados os requisitos, inicia-se a fase de análise de requisitos, fase esta que resulta no refinamento dos requisitos levantados. Durante a etapa de análise de requisitos, são construídos modelos gráficos para que haja melhor compreenssão do domínio e dos processos de negócio do sistema em desenvolvimento. Essa fase serve de base para a geração do Documento de Especificação de Requisitos~\cite{rfalbo:naer}.

Sucessivamente, temos a fase de implementação. Aqui o \textit{software} é verdadeiramente programado utilizando a linguagem de programação, \textit{frameworks} e demais tecnologias escolhidas durante a fase de análise de requisitos. A fase de testes, que deve ser feita durante todo o ciclo de implementação do WIS, visa garantir a qualidade do \textit{software}, dando enfoque a características como usabilidade, segurança, eficiência, interoperabilidade e outros, assim como validar se o mesmo foi ou está sendo construído de acordo com as necessidades do cliente. Satisfeitos os requisitos, o WIS deve ser implantado num \textit{web server} e disponibilizado para as partes interessadas.

\section{Frameworks}
\label{sec-frameworks}

\textit{Frameworks} são conjuntos de artefatos de código para reúso, que, uma vez combinados, podem formar uma infraestrutura de uma aplicação semi-completa. Mediante configurações, composições e heranças, estes frameworks podem ser utilizados como base para o desenvolvimento de aplicações Web que resolvam problemas dos mais diversos domínios~\cite{souza:masterthesis07}. Nas subseções que se seguem, são apresentados os tipos de \textit{frameworks} que foram utilizados para o desenvolvimento deste trabalho.

\subsection{Frameworks MVC (\textit{Model-View-Controller})}
\label{sec-frameworks-mvc}

O padrão arquitetural MVC (Model-View-Controller ou Modelo-Visão-Controlador) consiste na divisão da aplicação em 3 camadas, cada qual com diferentes competências. A Figura~\ref{fig-frameworks-mvc} apresenta o fluxo de eventos do padrão MVC.

\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{figuras/mvc.png}
	\caption{Funcionamento do padrão Modelo-Visão-Controlador.}
	\label{fig-frameworks-mvc}
\end{figure}

``Elementos da visão representam informações de modelo e as exibem ao usuário, que pode enviar, por meio deles, estímulos ao sistema, que são tratados pelo controlador. Este último altera o estado dos elementos do modelo e pode, se apropriado, selecionar outros elementos de visão a serem exibidos ao usuário. O modelo, por sua vez, notifica alterações em sua estrutura à visão para que esta consulte novamente os dados e se atualize automaticamente''~\cite{souza:masterthesis07}.

A camada de visão se encontra do lado do cliente, representada por páginas Web, enquanto que a camada de modelo está situada do lado de servidor, onde a aplicação Web é sustentada. Desta forma, para que a comunicação entre visão e modelo seja possível, é necessário um agente intermediador, neste caso, a camada de controle. Um \textit{framework} MVC é aquele que disponibiliza um serviço para receber e responder requisições HTTP (\textit{Hypertext Transfer Protocol}), possibilitando a comunicação cliente-servidor. Este serviço, por sua vez, pode ser estendido e especializado de acordo com as necessidades da aplicação e, comunentemente, oferece uma série de facilidades como: definição de rotas customizadas, conversão automática de tipos, validação de dados de entrada e etc.~\cite{souza:masterthesis07}.

\textit{Frameworks} MVC trazem como vantagem uma melhor modularização, coesão e manutebilidade do código da aplicação, evitando que o código da lógica de negócio esteja entremeado diretamente nas páginas Web, fazendo com que cada uma das partes cumpra apenas com as responsabilidades que lhe dizem respeito. 

\subsection{Frameworks ORM (\textit{Object/Relational Mapping})}
\label{sec-frameworks-orm}

Sistemas de Gerenciamento de Banco de Dados Relacionais (SGBDR) são comunentemente utilizados para armazenamento de informações na indústria de \textit{software}. Isso se dá pelo fato de terem uma base teórica bem consolidada no campo da álgebra relacional, garantindo-lhes uma maior confiabilidade. Porém, mesmo aplicações desenvolvidas sobre o paradigma de programação orientada a objetos (POO), fazem utilização de SGBDRs para persistência de dados. Esse ``choque'' entre paradigmas gera um problema de compatibilidade na conversação entre as duas vertentes, conhecido como \textit{paradigm mismatch} (ou incompatibilidade de paradigmas), visto que os bancos de dados que trabalham com o conceito de álgebra relacional, estruturam dados em tabelas, enquanto que aplicações que utilizam o paradigma POO, armazenam informações em grafos de objetos interconectados e referenciados em memória~\cite{souza:masterthesis07}.

Para solucionar este problema, surgiu na década de 1980 a ideia do Mapeamento Objeto/Relacional (\textit{Object/Relational Mapping} ou apenas ORM), que consiste na persistência de objetos em tabelas de SGBDRs de forma transparente, se dando através da utilização de meta-dados que descrevem o mapeamento entre o mundo dos objetos e o mundo relacional~\cite{king:orm2005,souza:masterthesis07}. A Figura~\ref{fig-orm} apresenta a ideia de funcionamento de um \textit{framework} ORM.

\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{figuras/orm.png}
	\caption{Funcionamento de um \textit{framework} ORM.}
	\label{fig-orm}
\end{figure}

De modo geral, ao invés do desenvolvedor interagir com o SGBDR utilizando SQL (\textit{Structured Query Language}, ou Linguagem de Consulta Estruturada) e ter o trabalho de mapear objetos e tabelas manualmente, cabe a ele apenas informar ao \textit{framework} ORM quais são as regras de mapeamento a serem utilizadas nos objetos (geralmente feitas com \textit{Data Annotations} ou \textit{Fluent API}), tornando possível a transformação deles (objetos) para o formato de tabelas e vice-versa. Além disso, as interações com o banco de dados são feitas através da execução de métodos, como \textit{add()}, \textit{find()}, \textit{update()} e \textit{remove()}, característicos de operações básicas de CRUD (\textit{Create, Read, Update, Delete}).

\subsection{Frameworks de Injeção de Dependência}
\label{sec-frameworks-di}

Uma vez que um WIS se encontra particionado em diferentes camadas, cada qual com suas responsabilidades, surge a necessidade de integrá-las para que se comuniquem e o sistema funcione como esperado. Segundo~\citeonline{fowler2006:di}, uma vez que uma classe instanciada depende diretamente de serviços providos por outra classe, é interessante que a primeira faça utilização da segunda através de um contrato, ou seja, uma interface. Isso faz com que a classe dependente não precise ter conhecimento a respeito da implementação do serviço requisitado, mas apenas dos métodos os quais a interface do serviço oferece.

Seguindo essa linha de raciocínio, surgiram os \textit{frameworks} de Injeção de Dependência (\textit{Dependency Injection}, ou DI), que têm como função satisfazer as dependências entre as classes do sistema, especificadas pelo próprio desenvolvedor através de configurações externas. A Figura~\ref{fig-di} apresenta o comportamento comum de um \textit{framework} DI.

\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{figuras/di.png}
	\caption{Funcionamento de um \textit{framework} DI.}
	\label{fig-di}
\end{figure}


Uma vez requisitado um contrato de serviço, seja por construtor ou via \textit{data annotation}, cabe ao \textit{framework} DI injetar alguma instância de classe que atende este contrato, definida previamente pelo desenvolvedor. Essa abordagem retira da classe solicitante a responsabilidade de buscar ou criar um objeto de interesse, sendo também conhecida como Inversão de Controle (\textit{Inversion of Control}, ou IoC), fazendo com que o controle de busca e criação de objetos seja feito por um elemento externo, que neste caso é próprio \textit{framework} DI~\cite{souza:masterthesis07}.

\subsection{Frameworks de Segurança}
\label{sec-frameworks-seguranca}

Uma característica comum em WISs são mecanismos de autenticação e autorização de usuários. Autenticar um usuário consiste em verificar se uma chave de acesso (geralmente um par de login e senha ou um \textit{token}) é válida para acessar o sistema. Autorizar, por sua vez, consiste em verificar qual é o nível de acesso (ou \textit{roles}) do usuário autenticado, permitindo que este use somente funcionalidades que lhe dizem respeito~\cite{souza:masterthesis07}.

Existem \textit{frameworks} de Segurança que proveem artifícios prontos para a utilização destes mecanismos, bastando usar a solução padrão, que geralmente não requer customizações, ou que uma classe de domínio, que represente um usuário, herde a classe de usuário disponibilizada pelo \textit{framework}. Abaixo estão listados alguns \textit{frameworks} de segurança para diferentes plataformas:

\begin{itemize}
	\item \href{https://docs.microsoft.com/aspnet/core/security/authentication/identity}{Identity}, para C\# (ASP.NET Core);
	\item \href{https://spring.io/projects/spring-security}{Spring Security}, para Java (Spring);
	\item \href{https://laravel.com/docs/master/authentication}{Laravel Auth}, para PHP (Laravel).
\end{itemize}

\section{FrameWeb}
\label{sec-frameweb}

FrameWeb trata-se de um método de desenvolvimento de projetos de software, mais especificamente WISs (\textit{Web Information Systems}). Este, por sua vez, define uma arquitetura padrão a qual, em linhas gerais, incorpora \textit{frameworks} de diferentes naturezas, de maneira a utilizá-los concomitantemente com um conjunto de modelos de projeto propostos, fazendo com que tais modelos tornem-se mais ricos e próximos da fase de implementação~\cite{souza-celebratingfalbo20}.

\subsection{Arquitetura de Software do FrameWeb}
\label{sec-frameweb-aquitetura}

A principal proposta do método FrameWeb concentra-se na fase de Projeto, tendo uma arquitetura lógica que divide o sistema em diferentes camadas, denominada padrão arquitetônico \textit{Service Layer}~\cite{fowler:service-layer-architecture}, apresentado na Figura~\ref{fig-frameweb-arquitetura}.

\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{figuras/frameweb-arquitetura.png}
	\caption{Padrão arquitetônico \textit{Service Layer}~\cite{souza:masterthesis07}.}
	\label{fig-frameweb-arquitetura}
\end{figure}

A primeira camada é denominada \textbf{Lógica de Apresentação}. Nesta, teremos os pacotes Visão e Controle. O pacote Visão é composto por páginas \textit{Web}, folhas de estilo, conteúdos multimídia e scripts, sendo disponibilizados do lado do cliente para interação com o usuário. O pacote Controle é composto por classes de ação, incorporadas por um \textit{framework} \textit{Front Controller} (ou Controlador Frontal)~\cite[p. 166]{alur2003:dao}, responsável por estabelecer a comunicação entre serviços e cliente, e isso se dá através de estímulos provocados pelo usuário, que refletem em alguma ação definida em uma classe de controle.

A segunda camada é denominada como \textbf{Lógica de Negócio}. Nesta, teremos os pacotes Domínio e Aplicação. O primeiro é composto por classes que representam o domínio do problema, sendo as mesmas geralmente definidas no diagrama de classes criado durante a fase de Especificação de Requisitos. O segundo é composto por classes de serviço, responsáveis por prover funcionalidades, geralmente definidas nos modelos de Casos de Uso criados durante a fase de Especificação de Requisitos.

O pacote Controle depende do pacote Aplicação para que as funcionalidades do sistema possam ser providas ao usuário, da mesma forma que o pacote Aplicação depende do pacote Domínio para manipular suas informações. Os pacotes Controle e Visão possuem uma relação de dependência com o pacote Domínio, todavia esteriotipada como \textbf{<<fraca>>}, representando um baixo acomplamento entre eles. Isso se dá pelo fato desses pacotes não modificarem objetos das classes de domínio (entidade) de forma persistente, tendo sempre o pacote Aplicação como intermediador dessa interação.

A terceira e última camada é denominada como \textbf{Lógica de Acesso a Dados}. Nesta, há apenas o pacote Persistência, que é composto por classes DAO (\textit{Data Access Object})~\cite{alur2003:dao} que utilizam algum \textit{framework} ORM (\textit{Object/Relational Mapping}) responsável por prover uma camada de abstração para manipulação de objetos no banco de dados relacional.

O pacote Aplicação depende do pacote de Persistência para que os objetos de entidade possam ser gravados, recuperados, modificados e excluídos do banco de dados. Há também uma relação esteriotipada como \textbf{<<fraca>>} entre o pacote Persistência e Domínio, visto que o primeiro precisa conhecer as classes de entidade para persistir seus objetos.

\subsection{Modelos FrameWeb}
\label{sec-frameweb-models}

Além da arquitetura apresentada na Seção~\ref{sec-frameweb-aquitetura}, devemos modelar os artefatos a serem implementados na fase seguinte (implementação), sendo assim, o método propõe quatro diferentes tipos de modelos, todos baseados no diagrama de classes da UML:

\begin{itemize}
	\item O \textbf{Modelo de Entidades} representa o domínio do problema através de um diagrama de classes da UML. Por sua vez, cada classe contém configurações que dizem respeito ao seu mapeamento para o banco de dados. A partir deste modelo são implementadas as classes do pacote Entidade na fase de implementação, pertencente a camada de Lógica de Negócio~\cite{souza-celebratingfalbo20};
	
	\item O \textbf{Modelo de Persistência} representa as classes DAO, se dando através de um diagrama de classes da UML. Cada DAO encarrega-se por persistir instâncias das classes de entidade no banco de dados. Estas classes fazem parte do pacote de Persistência, incluso na camada de Lógica de Acesso a Dados~\cite{souza-celebratingfalbo20};
	
	\item O \textbf{Modelo de Navegação} representa os componentes da camada de Lógica de Apresentação, se dando através de um diagrama de classes UML, são elas: páginas Web e formulários HTML, pertencentes ao pacote Visão; além das classes de ação (ou controle), que compõem o pacote Controle~\cite{souza-celebratingfalbo20};
	
	\item O \textbf{Modelo de Aplicação} é um diagrama de classes da UML responsável por representar as classes de serviço, que codificam os casos de uso, e suas dependências. Este diagrama guia a implementação das classes do pacote Aplicação e a configuração das dependências entre os pacotes Controle, Aplicação e Persistência, especificando quais classes de ação dependem de quais classes de serviço e quais DAOs atendem as classes de serviço~\cite{souza-celebratingfalbo20};
\end{itemize}

\subsection{Ferramentas FrameWeb}
\label{sec-frameweb-tools}

Em razão dos modelos FrameWeb serem baseados na UML, qualquer editor UML pode ser utilizado para construí-los. No entanto, para garantir que os modelos fossem elaborados utilizando apenas construtos válidos para o FrameWeb, surgiu a necessidade de desenvolver uma linguagem própria, especificada formalmente através de metamodelos~\cite{martins-souza:webmedia15}, utilizando uma abordagem MDD (\textit{Model-driven Development} ou Desenvolvimento Orientado a Modelos).

A partir do metamodelo do FrameWeb, foi construído o FrameWeb Editor~\cite{campos-souza:webmedia17}, ferramenta que oferece um ambiente gráfico baseado na plataforma Eclipse RCP (\textit{Rich Client Platform}) a qual viabiliza a criação dos diagramas dos modelos FrameWeb. A Figura~\ref{fig-frameweb-editor-overview} nos dá uma visão geral do editor, tendo, à esquerda, o \textit{Model Explorer}, utilizado para visualização de arquivos e diretórios do projeto. No centro estão as representações visuais dos modelos, \textit{frameworks} e configuração do projeto. À direita há uma paleta de construtos para serem utilizados nos modelos e, porfim, na parte inferior são disponibilizadas as opções de configuração dos elementos que compõe os modelos.

\begin{figure}
	\centering
\includegraphics[width=\columnwidth]{figuras/frameweb-editor-overview-first-version.png}
	\caption{Visão geral da primeira versão do FrameWeb Editor~\cite{campos-souza:webmedia17}.}
	\label{fig-frameweb-editor-overview}
\end{figure}

Para desenvolver aplicações com o FrameWeb \textit{Editor}, algumas ferramentas precisam ser instaladas. Primeiramente, é necessário ter instalado algum \textit{runtime} Java a partir da versão 8; Em seguida, instalar o \textit{Eclipse IDE for Java EE Developers}. Uma vez instalado o Eclipse, é preciso adicionar os \textit{plugins}: Sirius, através do \textit{Eclipse Marketplace}, e FrameWeb Tools, através da opção \textit{Help}, seguida de \textit{Install New Software}, aplicando o \textit{endpoint} \url{http://dev.nemo.inf.ufes.br/framewebplugin/}. Mais detalhes do processo de preparação do ambiente FrameWeb estão disponíveis no ToolsTutorial01.\footnote{\url{https://github.com/nemo-ufes/FrameWeb/wiki/ToolsTutorial01}}

Com os pré-requisitos concluídos, podemos criar um projeto Web (\textit{Dynamic Web Project}) no Eclipse e adicionar a faceta \textit{FrameWeb Tools}. Assim que a faceta for adicionada, será criado um modelo FrameWeb em branco, contendo apenas o componente FrameWeb Configuration. A Figura~\ref{fig-oldenburge-overview} apresenta uma visão geral do editor após a adição da faceta.

\begin{figure}
	\centering
	\includegraphics[width=\columnwidth]{figuras/oldenburg/oldenburg-overview.png}
	\caption{Projeto FrameWeb com a faceta ativada~\cite{souza-celebratingfalbo20}.}
	\label{fig-oldenburge-overview}
\end{figure}

O elemento nomeado como ``FrameWeb Configuration'' está atrelado ao arquivo Configuration.frameweb, que nos permite adicionar algumas configurações globais no projeto, são elas: \textit{Class Extension} e \textit{Page Extension} --- extensões dos arquivos de classes e páginas Web (ex: .cs e .html); \textit{Framework Definition Path} --- caminho para o diretório de templates; \textit{Src Path} e \textit{View Path} --- caminhos para os diretórios onde classes e páginas Web serão geradas, respectivamente.

Os demais elementos disponíveis no quadro principal do editor referem-se a definição de linguagem (\textit{Language Definition Files}), que, na prática, inclui uma lista de tipos primitivos e classes de uma determinada linguagem (ex: Java possui tipos int, char, String, etc.); e \textit{frameworks} (\textit{Framework Definition File}), associados a templates, que, no caso do JSF (\textit{framework} MVC), inclui também um conjunto de tags referentes aos componentes visuais da biblioteca de decoração (PrimeFaces tem inputText, commandButton, etc.), que serão utilizadas para geração de código do Modelo de Navegação. Estas definições estão disponíveis para \textit{download} no repositório oficial da ferramenta\footnote{\url{https://github.com/nemo-ufes/FrameWeb}} e foram importadas para o projeto.

Como exemplo, apresentaremos o Oldenburg, WIS para registro de autores de conferências, apresentado na \textit{wiki} do FrameWeb Editor.\footnote{\url{https://github.com/nemo-ufes/FrameWeb/wiki/ToolsTutorial02}} A Figura~\ref{fig-oldenburge-entity-model} apresenta o Modelo de Entidades do Oldenburg, contendo a classe \textbf{Author} e seu mapeamento objeto-relacional. Neste modelo estão definidos os atributos da entidade Author, cada qual com sua visibilidade, nome, tipo de dado e regras de mapeamento (ou \textit{constraints}). Não há especificado um atributo ID para a classe \textbf{Author}, pois as entidades do Oldenburg herdam a classe abstrata \textbf{PersistentObjectSupport}, pertencente ao pacote de utilitários do JButler.\footnote{\url{https://github.com/dwws-ufes/jbutler}}

\begin{figure}
	\centering
	\includegraphics[width=.6\columnwidth]{figuras/oldenburg/oldenburg-entity-model.png}
	\caption{Modelo de Entidades do Oldenburg~\cite{souza-celebratingfalbo20}.}
	\label{fig-oldenburge-entity-model}
\end{figure}

A persistência de \textbf{Author}, apresentada no Modelo de Persistência na Figura~\ref{fig-oldenburge-persistence-model}, é tratada pela interface \textbf{AuthorDAO}, que é implementada pela classe \textbf{AuthorDAOJPA}. Este DAO define o método \textit{retrieveByEmail}, retornando um autor (Author) a partir do email do mesmo. No Oldenburg, todas as interfaces DAO herdam \textbf{BaseDAO} e todas as classes DAO herdam \textbf{BaseJPADAO}, os quais fornecem métodos básicos de CRUD. Ambos encontram-se disponíveis no pacote de utilitários do JButler.

\begin{figure}
	\centering
	\includegraphics[width=.6\columnwidth]{figuras/oldenburg/oldenburg-persistence-model.png}
	\caption{Modelo de Persistência do Oldenburg~\cite{souza-celebratingfalbo20}.}
	\label{fig-oldenburge-persistence-model}
\end{figure}

As telas e classe de controle que dão acesso às funcionalidade de cadastro de autores são representadas pelo Modelo de Navegação, apresentado na Figura~\ref{fig-oldenburge-navigation-model}. Presente no diretório ``\textit{$\sim$/core/registration}'', os componentes \textit{Page}, assinados com o esteriótipo \textbf{<<page>>}, representam páginas Web, sendo \textit{\textbf{index}} a principal página da funcionalidade de cadastro de autor. A página \textit{\textbf{index}} é composta pelo formulário \textit{registrationForm}, assinado com o esteriótipo \textbf{<<form>>}, o qual contém campos de texto (\textit{inputText}) para nome e email do autor, assim como campos para preenchimento e confirmação de senha (\textit{password}). Este formulário se encontra associado à classe de controle \textbf{RegistrationController} e, assim que submetido, o Controlador Frontal irá copiar os valores dos campos recebidos pelo corpo da requisição para o atributo \textit{author} (note que os campos do formulário submetido possuem sufixo de mesmo nome do atributo de destino), presente na própria \textit{controller}, assim executando o método \textit{register}. A depender do resultado, o usuário será redirecionado para as páginas \textit{\textbf{success}} ou \textit{\textbf{emailinuse}}, devolvendo algumas informações úteis para montar a página de destino (\textit{author.name} ou \textit{author.email}).

\begin{figure}
	\centering
	\includegraphics[width=\columnwidth]{figuras/oldenburg/oldenburg-navigation-model.png}
	\caption{Modelo de Navegação do Oldenburg~\cite{souza-celebratingfalbo20}.}
	\label{fig-oldenburge-navigation-model}
\end{figure}

Para representar as interações entre as diferentes partes do WIS, utiliza-se o Modelo de Aplicação, apresentado na Figura~\ref{fig-oldenburge-application-model}. Assim como as interfaces e classes DAO mostradas anteriomente, os serviços também são divididos entre interface e implementação, a dizer, \textbf{RegistrationService} e \textbf{RegistrationServiceBean}, respectivamente. A controladora \textbf{RegistrationController} se encontra associada à interface \textbf{RegistrationService}, indicando uma relação de dependência que implicará na injeção do serviço solicitado na classe de controle. Este mesmo comportamento acontece a partir da relação de dependência entre \textbf{RegistrationServiceBean} e \textbf{AuthorDAO}, onde o DAO é injetado no serviço.

\begin{figure}
	\centering
	\includegraphics[width=\columnwidth]{figuras/oldenburg/oldenburg-application-model.png}
	\caption{Modelo de Aplicação do Oldenburg~\cite{souza-celebratingfalbo20}.}
	\label{fig-oldenburge-application-model}
\end{figure}

De modo geral, uma vez associadas as diferentes partes do WIS, assim que usuário submeter o formulário de cadastro de autor na camada de visão, este será capturado pela \textit{controller} \textbf{RegistrationController}, que irá invocar o serviço \textbf{RegistrationService} e executar o método \textit{register}. Este método, por sua vez, utiliza \textbf{AuthorDAO} para performar as operações necessárias no banco de dados e prover uma resposta para o usuário.

Criados os modelos FrameWeb, pode-se então gerar o código do WIS clicando com o botão direito em algum espaço em branco do quadro principal do editor, então selecionando a opção ``Generate Source Code''. Os arquivos referentes às interfaces, classes e páginas Web serão criados em seus respectivos diretórios dentro do projeto Eclipse.

A Listagem~\ref{lst-oldenburg-controller-template} (extraída do repositório oficial do FrameWeb\footnote{\url{https://github.com/nemo-ufes/FrameWeb/blob/master/frameworks/jbutler/templates}}) apresenta o template utilizado para a geração de código das classes Front Controller do JButler para o Oldenburg. A notação \textbf{\{\{\ var \}\}} demarca o local a qual será inserido o valor de alguma variável ou atributo extraídos do Modelo de Navegação, enquanto \textbf{\{\% if | for | endif | ... \%\}} indica o início ou término de \textit{statements} de fluxo de controle. No repositório oficial do editor há uma tabela que aponta quais variáveis estão disponíveis para utilização em cada modelo/\textit{template}.\footnote{\url{https://github.com/nemo-ufes/FrameWeb/tree/master/frameworks/utils}} Todas as \textit{tags}, filtros e funções do Twig (versão 1.x) encontram-se disponíveis na documentação oficial da linguagem.\footnote{\url{https://twig.symfony.com/doc/1.x/}}

\newpage
\begin{lstlisting}[language={Java}, caption={Template para classes do tipo Front Controller}, label={lst-oldenburg-controller-template}]
package {{ package.Name }};

import javax.ejb.EJB;
import javax.enterprise.inject.*;
import br.ufes.inf.nemo.jbutler.ejb.controller.JSFController;

/** TODO: generated by FrameWeb. Should be documented. */
@Model
public class {{ class.Name }} extends JSFController {
	/** Serialization id (using default value, change if necessary). */
	private static final long serialVersionUID = 1L;
	
	{% for association in associations %}
	/** TODO: generated by FrameWeb. Should be documented. */
	@EJB
	private {{ association.TargetMember.Type.Name }} {{ association.TargetMember.Type.Name | lower_first }};
	{% endfor %}
			
	{% for attribute in attributes %}
	/** TODO: generated by FrameWeb. Should be documented. */
	private {{ attribute.Type.Name }} {{ attribute.Name }};
	{% endfor %}
			
	{% for method in methods %}
	/** TODO: generated by FrameWeb. Should be documented. */
	{{ method.Visibility.Name }} {% if method.MethodType is null %}void{% else %}{{ method.MethodType.Name }}{% endif %} {{ method.Name }}({% for parameter in method.OwnedParameters %}{{ parameter.Type.Name }} {{ parameter.Name }}{% if loop.last == false %}, {% endif %}{% endfor %}) {
		// FIXME: auto-generated method stub
		return{% if method.MethodType is not null %} null{% endif %};
	}
	{% endfor %}
						
	{% for attribute in attributes %}
	/** Getter for {{ attribute.Name }}. */
	public {{ attribute.Type.Name }} get{{ attribute.Name | capitalize }}() {
		return {{ attribute.Name }};
	}
							
	/** Setter for {{ attribute.Name }}. */
	public void set{{ attribute.Name | capitalize }}({{ attribute.Type.Name }} {{ attribute.Name }}) {
		this.{{ attribute.Name }} = {{ attribute.Name }};
	}
	{% endfor %}
}
\end{lstlisting}
